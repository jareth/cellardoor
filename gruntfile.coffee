module.exports = (grunt)->

  grunt.initConfig
    pkg: grunt.file.readJSON('package.json')

    coffeelint:
      app: ['app/**/*.coffee', 'gruntfile.coffee']

    coffee:
      compile:
        files:
          'www/js/app.js': [
            'app/site/CellarDoorApp.coffee',
            'app/site/CellarDoorCtrls.coffee'
          ]

    jshint:
      all: ['www/js/**/*.js', 'www/javascripts/**/*.js']

    uglify:
      options:
        compress:
          drop_console: true
      app:
        files:
          'www/js/app.min.js': ['www/js/app.js']

    sass:
      dist:
        options:
          includePaths: ['www/bower_components/foundation/scss']
          outputStyle: 'compressed'
        files:
          'www/css/app.css': 'app/app.scss'

    express:
      coffee:
        options:
          script: 'app.js'
          #cmd: 'coffee'
          port: process.env.PORT

    watch:
      grunt: 'gruntfile.coffee'

      sass:
        files: ['app/**/*.scss']
        tasks: ['sass']

      express:
        files: ['app/**/*.coffee']
        tasks: ['coffee', 'express:coffee']
        options:
          spawn: false


  grunt.loadNpmTasks 'grunt-coffeelint'
  grunt.loadNpmTasks 'grunt-contrib-coffee'
  grunt.loadNpmTasks 'grunt-contrib-jshint'
  grunt.loadNpmTasks 'grunt-contrib-uglify'
  grunt.loadNpmTasks 'grunt-sass'
  grunt.loadNpmTasks 'grunt-contrib-watch'
  grunt.loadNpmTasks 'grunt-express-server'

  grunt.registerTask 'prod', ['build', 'uglify']
  grunt.registerTask 'dev', ['build', 'express:coffee', 'watch']
  grunt.registerTask 'build', ['coffeelint', 'coffee', 'jshint', 'sass']
  grunt.registerTask 'default', ['dev']
