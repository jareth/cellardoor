express = require 'express'

index = (req, res)->
  res.render 'index',
    title: 'Cellar Door',
    page:  'index'

home = (req, res)->
  res.render 'partials/home',
    title: 'Cellar Door',
    page:  'home'

module.exports = (app) ->
  app.get '/', index
  app.get '/home', home