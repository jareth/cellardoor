cellarDoorApp = angular.module 'CellarDoorApp', [
  'CellarDoorCtrls',
  'ngRoute'
]

cellarDoorApp.config [
  '$routeProvider',
  ($routeProvider) ->
    $routeProvider.
      when '/',
        templateUrl: '/home',
        controller: 'tourListCtrl'
      .otherwise redirectTo: '/'
    return
]