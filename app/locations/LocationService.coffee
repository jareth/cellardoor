mongo = require 'mongoskin'

db = mongo.db 'jareth:P3nguins@paulo.mongohq.com:10023/CellarDoor'
locations = db.collection 'locations'

getLocations = (callback) ->
  locations.find({},{name:1, address:1, loc:1}).toArray (error, results) ->
    if error
      callback error
    else
      callback null, results

getDetails = (queryId, callback) ->
  locations.find(
    _id: db.ObjectID.createFromHexString(queryId)
  ).toArray (error, results) ->
    if error
      callback error
    else
      callback null, results


module.exports =
  getLocations: getLocations
  getDetails: getDetails
