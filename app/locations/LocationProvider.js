var mongo = require("mongoskin");
var gm = require('googlemaps');

var db = mongo.db('jareth:P3nguins@paulo.mongohq.com:10023/CellarDoor');
var locations = db.collection('locations');
var distances = db.collection('distances');

function findAll(callback) {
    locations.find().limit(5).toArray(function(error, results) {
        if (error) callback(error);
        else callback(null, results);
    });
}

function findSome(limit, skip, callback) {
    locations.count(function(error, countResults) {
        locations.find().skip(skip).limit(limit).toArray(function(error, findResults) {
            if (error) callback(error);
            else callback(null, {count:countResults, current:skip, found:findResults});
        });
    });
}

function findOne(loc_id, callback) {
    locations.find( {_id: db.ObjectID.createFromHexString(loc_id) } ).toArray(function(error, results) {
        if (error) callback(error);
        else callback(null, results);
    });
}

function save(new_locations, callback) {
    // find nearby to calc distances
    findNear(new_locations.loc, 100000, function(err, nearResults) {
        if (err) {
            console.log("google distance api lookup failed.");
            console.log(err);
            callback(err);
            return;
        } else {
            
            // calc distances
            gm.distance(new_locations.loc.lat + "," + new_locations.loc.lon
            , prepArrayForDistanceMatrix(nearResults)
            , function(err, result) {
                
                if (err) {
                    console.log("error getting distances");
                    callback(err);
                    return;
                }
                // add origin address to new_locations
                new_locations.address = result.origin_addresses[0];
                
                var insertResult;
                locations.insert(new_locations, function(err, insertResult) {
                    if (err) {
                        console.log("failed to insert location");
                        callback(err);
                        return;
                    } else {
                        new_locations._id = insertResult[0]._id;
                    }
                });
                
                var newDistances = [];
                
                // distances / times
                for (var i = 0; i < result.rows[0].elements.length; i++) {
                    newDistances.push({
                        locations: [new_locations._id, nearResults[i]._id]
                        , distance: result.rows[0].elements[i].distance.value
                        , duration: result.rows[0].elements[i].duration.value
                    });
                }
                distances.insert(newDistances, function(err, result) {
                    if (err) console.log("error inserting distances");
                });
                
                callback(null, insertResult);
            });
        }
    });
}

function prepArrayForDistanceMatrix(arr) {
    var str = "";
    for (var i=0; i < arr.length; i++) {
        str += arr[i].loc.lat + "," + arr[i].loc.lon + "|";
    }
    return str;
}

function remove(delete_id, callback) {
    delete_id = db.ObjectID.createFromHexString(delete_id);
    locations.remove({_id: delete_id}, 1, function(err, result) {
        distances.remove( {locations: delete_id }, 0, function(err, result) {
            console.log("removed? " + err + " " + result);
            callback(err, result);
        });
    });
}

function findNear(location, distance, callback) {
    locations.find( {
        loc: {
            $near: { 
                $geometry: { 
                    type : "Point" ,
                    coordinates : [
                        location.lon, 
                        location.lat
                    ]
                }, 
                $maxDistance: distance
            }
        }
    }).toArray(function(error, results) {
        callback(error, results);
    });
}

function getDistances(locationIds, callback) {
    distances.find( {
       locations: {
           $in: locationIds
       } 
    }).sort({duration: 1}).toArray(function(error, results) {
        callback(error, results);
    });
}

module.exports = {
    findAll: findAll
    , findSome: findSome
    , findOne: findOne
    , save: save
    , remove: remove
    , findNear: findNear
    , getDistances: getDistances
};