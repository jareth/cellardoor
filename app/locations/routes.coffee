express = require 'express'
locationService = require './LocationService.coffee'

getLocations = (req, res)->
  locationService.getLocations (error, results) ->
    if error
      console.log error
      res.status(404).send JSON.stringify error
    else
      res.json results

getDetails = (req, res)->
  locationService.getDetails req.param('locationId'), (error, results) ->
    if error
      console.log error
      res.status(404).send JSON.stringify error
    else
      res.json results


module.exports = (app) ->
  app.get '/locations/list', getLocations
  app.get '/locations/:locationId', getDetails