var locationStruct = [
    {"name" : "ID", "type" : "ObjectId", "key" : "_id"},
    {"name" : "Name", "type" : "text", "key" : "name", "list": true},
    {"name" : "Location", "type" : "latlon", "key" : "location"},
    {"name" : "Region", "type" : "text", "key" : "region"},
    {"name" : "Rating", "type" : "rating", "key" : "rating"},
    {"name" : "website", "type" : "url", "key" : "website"},
    {"name" : "Address", "type" : "text", "key" : "address"},
    {"name" : "Phone", "type" : "tel", "key" : "phone"},
    {"name" : "hours", "type" : "time", "key" : "hours"}
];

var locationProvider = require("../LocationProvider");

/*
 * GET home page.
 */
exports.index = function(req, res){
  res.render('admin', { title: 'Express' , page:'discover' })
};

/*
 * GET locations page
 */
exports.locations = function(req, res){
    locationProvider.findSome(5, 0, function( error, locs) {
        if (error) res.render('locationsadmin', {
            title: 'Locations', 
            page:'locations', 
            structure: locationStruct,
            data: error
        });
        else res.render('locationsadmin',
            {title: 'Locations', 
            page:'locations', 
            structure: locationStruct, 
            data: locs
        });
    });
}

/*
 * GET locations page
 */
exports.changePage = function(req, res){
    var pageLength = 10;
    var page = parseInt(req.param('page')) - 1;
    locationProvider.findSome(pageLength, page * pageLength, function( error, locs) {
        if (error) {
            console.log(error);
            res.status(404).send(JSON.stringify(error));
        }
        else res.render('list', {
            pageLength: pageLength, 
            structure: locationStruct,
            data: locs
        });
    });
}

/*
 * GET location Data
 */
exports.locationData = function(req, res){
    locationProvider.findOne(req.param('location'), function( error, loc) {
        if (error) {
            console.log(error);
            res.status(404).send(JSON.stringify(error));
        }
        else res.json(loc)
    });
}

/*
 * GET delete location page
 */
exports.remove = function(req, res){
    locationProvider.remove(req.params.id, function( error, locs) {
        if (error) 
            console.log("failed to remove id " + req.params.id);
        exports.locations(req, res);
    });
}

/*
 * POST insert location page
 */
exports.add = function(req, res){
    var data = {};
    for (var i = 0; i < locationStruct.length; i++) {
        if (locationStruct[i].key != "_id") {
            if (locationStruct[i].key == "location") {
                data.loc = {"lon": parseFloat(req.body.lon), "lat": parseFloat(req.body.lat)};
            } else {
                data[locationStruct[i].key] = req.body[locationStruct[i].key];
            }
        }
    }
    
    locationProvider.save(data, function(error, docs) {
        if (error) 
            console.log("failed to save location " + error);
        exports.locations(req, res);
    });
}