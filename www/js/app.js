(function() {
  var cellarDoorApp;

  cellarDoorApp = angular.module('CellarDoorApp', ['CellarDoorCtrls', 'ngRoute']);

  cellarDoorApp.config([
    '$routeProvider', function($routeProvider) {
      $routeProvider.when('/', {
        templateUrl: '/home',
        controller: 'tourListCtrl'
      }).otherwise({
        redirectTo: '/'
      });
    }
  ]);

}).call(this);

(function() {
  var cellarDoorCtrls;

  cellarDoorCtrls = angular.module('CellarDoorCtrls', []);

  cellarDoorCtrls.controller('tourListCtrl', [
    '$scope', function($scope) {
      $scope.hello = 'world';
    }
  ]);

}).call(this);
